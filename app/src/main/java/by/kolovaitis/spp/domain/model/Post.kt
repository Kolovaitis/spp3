package by.kolovaitis.spp.domain.model

data class Post(val name: String, val image: String?, val description:String, val id:Int?=null)