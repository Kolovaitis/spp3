package by.kolovaitis.spp.domain.repository

import by.kolovaitis.spp.domain.model.Comment
import by.kolovaitis.spp.domain.model.Post

interface CommentsRepository {
    suspend fun getComments(postId:Int): List<Comment>
    suspend fun addComment(postId: Int, text:String)
}