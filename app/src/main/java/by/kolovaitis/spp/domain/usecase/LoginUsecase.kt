package by.kolovaitis.spp.domain.usecase

import by.kolovaitis.spp.domain.repository.LoginRepository

class LoginUsecase(private val loginRepository: LoginRepository) {
    suspend operator fun invoke(email:String, password:String){
        loginRepository.login(email, password)
    }
}