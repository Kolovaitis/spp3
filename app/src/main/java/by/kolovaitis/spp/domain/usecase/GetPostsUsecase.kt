package by.kolovaitis.spp.domain.usecase

import by.kolovaitis.spp.domain.model.Post
import by.kolovaitis.spp.domain.repository.PostsRepository

class GetPostsUsecase(private val postsRepository: PostsRepository) {
    suspend operator fun invoke():List<Post>{
        return postsRepository.getPosts()
    }
}