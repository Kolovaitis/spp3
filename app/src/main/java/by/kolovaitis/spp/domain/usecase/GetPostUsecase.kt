package by.kolovaitis.spp.domain.usecase

import by.kolovaitis.spp.domain.model.Post
import by.kolovaitis.spp.domain.repository.PostsRepository

class GetPostUsecase(private val postsRepository: PostsRepository) {
    suspend operator fun invoke(postId:Int):Post{
        return postsRepository.getPost(postId)
    }
}