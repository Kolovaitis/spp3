package by.kolovaitis.spp.domain.model

data class Comment (val postId:Int, val text:String, val author:String)