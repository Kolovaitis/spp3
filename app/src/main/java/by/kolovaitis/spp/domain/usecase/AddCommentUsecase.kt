package by.kolovaitis.spp.domain.usecase

import by.kolovaitis.spp.domain.repository.CommentsRepository

class AddCommentUsecase(private val commentsRepository: CommentsRepository) {
    suspend operator fun invoke(text: String, postId: Int) {
        commentsRepository.addComment(text = text, postId = postId)
    }
}