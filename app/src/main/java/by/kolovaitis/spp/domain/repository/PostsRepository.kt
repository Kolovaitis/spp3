package by.kolovaitis.spp.domain.repository

import by.kolovaitis.spp.domain.model.Post

interface PostsRepository {
    suspend fun getPosts():List<Post>
    suspend fun getPost(postId:Int):Post
    suspend fun addPost(post: Post)
}
