package by.kolovaitis.spp.domain.usecase

import by.kolovaitis.spp.domain.model.Comment
import by.kolovaitis.spp.domain.repository.CommentsRepository

class GetCommentsUsecase(private val commentsRepository: CommentsRepository) {
    suspend operator fun invoke(postId:Int):List<Comment>{
        return commentsRepository.getComments(postId)
    }
}