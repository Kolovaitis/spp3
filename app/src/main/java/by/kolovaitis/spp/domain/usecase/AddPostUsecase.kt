package by.kolovaitis.spp.domain.usecase

import by.kolovaitis.spp.domain.model.Post
import by.kolovaitis.spp.domain.repository.PostsRepository

class AddPostUsecase(private val postsRepository: PostsRepository) {
    suspend operator fun invoke(image: String, name: String, description: String) {
        postsRepository.addPost(Post(name = name, description = description, image = image))
    }
}