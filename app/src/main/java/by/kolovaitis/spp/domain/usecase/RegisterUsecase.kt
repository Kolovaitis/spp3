package by.kolovaitis.spp.domain.usecase

import by.kolovaitis.spp.domain.repository.LoginRepository

class RegisterUsecase(private val loginRepository: LoginRepository) {
    suspend operator fun invoke(email:String, password:String){
        loginRepository.register(email, password)
    }
}