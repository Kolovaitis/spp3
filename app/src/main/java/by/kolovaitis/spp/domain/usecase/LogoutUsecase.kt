package by.kolovaitis.spp.domain.usecase

import by.kolovaitis.spp.domain.repository.LoginRepository

class LogoutUsecase(private val loginRepository: LoginRepository) {
    suspend operator fun invoke(){
        loginRepository.logout()
    }
}