package by.kolovaitis.spp.data.model

data class LoginData (val email:String, val password:String)
data class LoginResponseData(val token:String)