package by.kolovaitis.spp.data.repository.impl

import by.kolovaitis.spp.data.datasource.PostsDatasource
import by.kolovaitis.spp.data.model.NewCommentData
import by.kolovaitis.spp.domain.model.Comment
import by.kolovaitis.spp.domain.repository.CommentsRepository

class RetrofitCommentsRepository(private val postsDatasource: PostsDatasource) :
    CommentsRepository {
    override suspend fun getComments(postId: Int): List<Comment> {
        return postsDatasource.getComments(postId).map {
            Comment(postId = it.postId, author = it.author ?: "", text = it.text ?: "")
        }
    }

    override suspend fun addComment(postId: Int, text: String) {
        postsDatasource.addComment(postId, NewCommentData(text = text))
    }

}