package by.kolovaitis.spp.data

import by.kolovaitis.spp.data.model.*
import by.kolovaitis.spp.domain.model.Post
import retrofit2.http.*

interface RetrofitServices {
    @GET("/all")
    suspend fun getPosts(@Header("Authorization") token: String): List<PostData>

    @GET("/post/{postId}")
    suspend fun getPost(
        @Path(value = "postId") postId: Int,
        @Header("Authorization") token: String,
    ): PostData

    @GET("/post/comments/{postId}")
    suspend fun getComments(
        @Path(value = "postId") postId: Int, @Header("Authorization") token: String,
    ): List<CommentData>

    @Headers("Content-Type: application/json")
    @POST("/post")
    suspend fun addPost(
        @Body post: NewPostData,
        @Header("Authorization") token:String
    )
    @Headers("Content-Type: application/json")
    @POST("/comment/{postId}")
    suspend fun addComment(
        @Path(value = "postId") postId: Int,
        @Body comment: NewCommentData,
        @Header("Authorization") token:String
    )
    @Headers("Content-Type: application/json")
    @POST("/registration")
    suspend fun registration(
        @Body body:LoginData
    ):LoginResponseData

    @Headers("Content-Type: application/json")
    @POST("/login")
    suspend fun login(
        @Body body:LoginData
    ):LoginResponseData
}