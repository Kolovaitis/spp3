package by.kolovaitis.spp.data.repository.impl

import by.kolovaitis.spp.data.datasource.PostsDatasource
import by.kolovaitis.spp.data.model.LoginData
import by.kolovaitis.spp.domain.repository.LoginRepository

class RetrofitLoginRepository(private val postsDatasource: PostsDatasource) : LoginRepository {
    override suspend fun login(email: String, password: String) {
        postsDatasource.login(LoginData(email = email, password = password))
    }

    override suspend fun logout() {
        postsDatasource.logout()
    }

    override suspend fun register(email: String, password: String) {
        postsDatasource.register(LoginData(email = email, password = password))
    }

}