package by.kolovaitis.spp.data.model

data class CommentData(val postId:Int, val text:String?, val author:String?)
data class NewCommentData(val text:String?)