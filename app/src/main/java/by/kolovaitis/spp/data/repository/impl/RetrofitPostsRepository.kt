package by.kolovaitis.spp.data.repository.impl

import by.kolovaitis.spp.data.datasource.PostsDatasource
import by.kolovaitis.spp.data.model.NewPostData
import by.kolovaitis.spp.domain.model.Post
import by.kolovaitis.spp.domain.repository.PostsRepository

class RetrofitPostsRepository(private val postsDatasource: PostsDatasource) : PostsRepository {
    override suspend fun getPosts(): List<Post> {
        return postsDatasource.getPosts().map {
            Post(
                name = it.name ?: "",
                image = it.image,
                description = it.description ?: "",
                id = it.id,
            )
        }
    }

    override suspend fun getPost(postId: Int): Post {
        return with(postsDatasource.getPost(postId)) {
            Post(name = name ?: "", id = id, image = image, description = description ?: "")
        }
    }

    override suspend fun addPost(post: Post) {
        postsDatasource.addPost(NewPostData(post.name, post.image, post.description))
    }
}