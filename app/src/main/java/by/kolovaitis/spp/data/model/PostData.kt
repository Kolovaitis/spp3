package by.kolovaitis.spp.data.model

data class PostData(val name: String?, val image: String?, val description:String?, val id:Int)
data class NewPostData(val name: String?, val image: String?, val description:String?)