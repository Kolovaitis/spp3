package by.kolovaitis.spp.data.datasource

import by.kolovaitis.spp.data.model.*

interface PostsDatasource {
    suspend fun getPosts(): List<PostData>
    suspend fun getPost(postId: Int): PostData
    suspend fun addPost(post: NewPostData)
    suspend fun getComments(postId: Int): List<CommentData>
    suspend fun addComment(postId: Int, newCommentData: NewCommentData)
    suspend fun logout()
    suspend fun login(credentials: LoginData)
    suspend fun register(credentials: LoginData)
}