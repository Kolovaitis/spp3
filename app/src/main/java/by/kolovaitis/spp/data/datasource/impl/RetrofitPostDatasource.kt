package by.kolovaitis.spp.data.datasource.impl

import android.system.Os.socket
import android.util.Log
import by.kolovaitis.spp.data.RetrofitServices
import by.kolovaitis.spp.data.datasource.PostsDatasource
import by.kolovaitis.spp.data.model.*
import com.apollographql.apollo.ApolloClient
import com.apollographql.apollo.coroutines.await
import com.example.CreatePostMutation
import com.example.GetAllPostsQuery
import com.google.gson.Gson
import io.socket.client.IO
import kotlinx.coroutines.delay


class RetrofitPostDatasource(
    private val services: RetrofitServices,
    private val apiUrl: String,
    private val apolloClient: ApolloClient
) : PostsDatasource {
    private lateinit var token: String
    override suspend fun getPosts(): List<PostData> {
        val response = apolloClient.query(GetAllPostsQuery()).await()
        val posts = response.data?.allPosts
        return posts?.map { post ->
            PostData(
                name = post.name(),
                image = post.image(),
                description = post.description(),
                id = post.id() ?: 0
            )
        } ?: emptyList()
    }

    override suspend fun getPost(postId: Int): PostData {
        val options: IO.Options = IO.Options()
        var result: PostData? = null
        val socket = IO.socket(apiUrl, options)
        socket.on("post") {
            result = Gson().fromJson(it[0].toString(), PostData::class.java)

        }
        socket.connect()
        socket.emit("post", postId)
        while (result == null) {
            delay(100)
        }
        return result as PostData
    }

    override suspend fun addPost(post: NewPostData) {
         apolloClient.mutate(CreatePostMutation(post.name!!, post.description!!, post.image!!)).await()

    }

    override suspend fun getComments(postId: Int): List<CommentData> {
        return services.getComments(postId, token = token)
    }

    override suspend fun addComment(postId: Int, newCommentData: NewCommentData) {
        services.addComment(postId, newCommentData, token = token)
    }

    override suspend fun logout() {
        token = ""
    }

    override suspend fun login(credentials: LoginData) {
        token = services.login(credentials).token
    }

    override suspend fun register(credentials: LoginData) {
        token = services.registration(credentials).token
    }
}