package by.kolovaitis.spp.modules

import by.kolovaitis.spp.data.datasource.PostsDatasource
import by.kolovaitis.spp.data.datasource.impl.RetrofitPostDatasource
import by.kolovaitis.spp.data.repository.impl.RetrofitCommentsRepository
import by.kolovaitis.spp.data.repository.impl.RetrofitLoginRepository
import by.kolovaitis.spp.data.repository.impl.RetrofitPostsRepository
import by.kolovaitis.spp.domain.repository.CommentsRepository
import by.kolovaitis.spp.domain.repository.LoginRepository
import by.kolovaitis.spp.domain.repository.PostsRepository
import by.kolovaitis.spp.domain.usecase.*
import by.kolovaitis.spp.presentation.list.ListViewModel
import by.kolovaitis.spp.presentation.login.LoginViewModel
import by.kolovaitis.spp.presentation.post.PostViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module
import by.kolovaitis.spp.BuildConfig
import com.apollographql.apollo.ApolloClient


val componentsModule = module {
    single<PostsDatasource> { RetrofitPostDatasource(services = get(),BuildConfig.API_URL, get()) }
    single<PostsRepository> { RetrofitPostsRepository(postsDatasource = get()) }
    single<CommentsRepository> { RetrofitCommentsRepository(postsDatasource = get()) }
    single<LoginRepository> { RetrofitLoginRepository(postsDatasource = get()) }
    factory { GetPostsUsecase(postsRepository = get()) }
    factory { GetPostUsecase(postsRepository = get()) }
    factory { GetCommentsUsecase(commentsRepository = get()) }
    factory { LoginUsecase(get()) }
    factory { RegisterUsecase(get()) }
    factory { LogoutUsecase(get()) }
    factory { AddPostUsecase(get()) }
    factory { AddCommentUsecase(get()) }

    viewModel { ListViewModel(getPosts = get(), get()) }
    viewModel { PostViewModel(getPost = get(), getComments = get(), get()) }
    viewModel { LoginViewModel(get(), get()) }

}