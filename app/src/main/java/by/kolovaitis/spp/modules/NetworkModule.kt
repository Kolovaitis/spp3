package by.kolovaitis.spp.modules

import by.kolovaitis.spp.BuildConfig
import by.kolovaitis.spp.data.RetrofitServices
import com.apollographql.apollo.ApolloClient
import okhttp3.OkHttpClient
import org.koin.dsl.module
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

val networkModule = module {
    factory { provideOkHttpClient() }
    factory { provideRetrofitServices(get()) }
    single { provideRetrofit(get()) }
    single{
        ApolloClient.builder()
            .serverUrl("https://limitless-refuge-92786.herokuapp.com/graphql")
            .okHttpClient(get())
            .build()
    }
}

fun provideRetrofit(okHttpClient: OkHttpClient): Retrofit {
    return Retrofit.Builder().baseUrl(BuildConfig.API_URL).client(okHttpClient)
        .addConverterFactory(GsonConverterFactory.create()).build()
}

fun provideOkHttpClient(): OkHttpClient {
    return OkHttpClient().newBuilder().build()
}

fun provideRetrofitServices(retrofit: Retrofit): RetrofitServices = retrofit.create(RetrofitServices::class.java)
