package by.kolovaitis.spp.presentation.base

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import java.lang.Exception

open class BaseViewModel : ViewModel() {
    private val _error: MutableLiveData<String> = MutableLiveData()
    val error: LiveData<String> get() = _error

    private val _isLoading: MutableLiveData<Boolean> = MutableLiveData()
    val isLoading: LiveData<Boolean> get() = _isLoading

    internal suspend inline fun runSafe(code: () -> Unit) {
        _isLoading.postValue(true)
        try {
            code()
        } catch (e: Exception) {
            _error.postValue(e.message)
        }
        _isLoading.postValue(false)
    }
}