package by.kolovaitis.spp.presentation.post

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import by.kolovaitis.spp.R
import by.kolovaitis.spp.domain.model.Comment
import by.kolovaitis.spp.domain.model.Post
import com.squareup.picasso.Picasso

class CommentsRecyclerViewAdapter(private var comments: List<Comment>) :
    RecyclerView.Adapter<CommentsRecyclerViewAdapter.CommentViewHolder>() {
    class CommentViewHolder(val view: View) : RecyclerView.ViewHolder(view) {
        val tvAuthor: TextView by lazy {
            view.findViewById<TextView>(R.id.tvAuthor)
        }
        val tvText: TextView by lazy {
            view.findViewById<TextView>(R.id.tvText)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CommentViewHolder {
        val itemView =
            LayoutInflater.from(parent.context).inflate(R.layout.item_comment, parent, false)
        return CommentViewHolder(itemView)
    }

    override fun getItemCount() = comments.size

    override fun onBindViewHolder(holder: CommentViewHolder, position: Int) {
        holder.tvText.text = comments[position].text
        holder.tvAuthor.text = comments[position].author
   }

    fun refreshData(data: List<Comment>) {
        comments = data
        this.notifyDataSetChanged()
    }
}