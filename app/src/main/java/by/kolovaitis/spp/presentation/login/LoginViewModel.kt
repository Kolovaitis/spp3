package by.kolovaitis.spp.presentation.login

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import by.kolovaitis.spp.domain.usecase.LoginUsecase
import by.kolovaitis.spp.domain.usecase.RegisterUsecase
import by.kolovaitis.spp.presentation.base.BaseViewModel
import kotlinx.coroutines.launch

class LoginViewModel(
    private val loginUsecase: LoginUsecase,
    private val registerUsecase: RegisterUsecase
) : BaseViewModel() {
    private val _isAuthorized: MutableLiveData<Boolean> by lazy{
        MutableLiveData()
    }
    val isAuthorize: LiveData<Boolean> get() = _isAuthorized

    fun login(email: String, password: String) {
        viewModelScope.launch {
            runSafe {
                loginUsecase(email, password)
                _isAuthorized.postValue(true)
            }
        }
    }
    fun register(email: String, password: String) {
        viewModelScope.launch {
            runSafe {
                registerUsecase(email, password)
                _isAuthorized.postValue(true)
            }
        }
    }
}