package by.kolovaitis.spp.presentation.post

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import by.kolovaitis.spp.R
import com.afollestad.materialdialogs.MaterialDialog
import com.afollestad.materialdialogs.bottomsheets.BottomSheet
import com.afollestad.materialdialogs.customview.customView
import com.afollestad.materialdialogs.customview.getCustomView
import com.afollestad.materialdialogs.lifecycle.lifecycleOwner
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.fragment_second.*
import kotlinx.android.synthetic.main.fragment_second.progressBar
import org.koin.androidx.viewmodel.ext.android.viewModel

/**
 * A simple [Fragment] subclass as the second destination in the navigation.
 */
class SecondFragment : Fragment() {
    private val viewModel: PostViewModel by viewModel()
    private lateinit var commentsAdapter:CommentsRecyclerViewAdapter
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_second, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        commentsAdapter = CommentsRecyclerViewAdapter(listOf())
        rvComments.layoutManager = LinearLayoutManager(context)
        rvComments.adapter = commentsAdapter
        observeViewModel()
        ivBack.setOnClickListener {
            activity?.onBackPressed()
        }
        fabAddComment.setOnClickListener {
            addComment()
        }

    }
    private fun addComment(){
        MaterialDialog(requireContext(), BottomSheet()).show {
            setTitle("Add comment")

            customView(R.layout.add_comment_layout, scrollable = true, horizontalPadding = true)
            positiveButton { dialog ->
                // Pull the password out of the custom view when the positive button is pressed
                val customView: View = dialog.getCustomView()
                val text = customView.findViewById<EditText>(R.id.etComment).text.toString()
                viewModel.addComment(id=arguments?.getInt("id"), text=text)
            }
            negativeButton(android.R.string.cancel)
            lifecycleOwner(viewLifecycleOwner)
        }
    }

    private fun observeViewModel() {
        viewModel.load(arguments?.getInt("id"))
        viewModel.loadComments(arguments?.getInt("id"))
        viewModel.error.observe(viewLifecycleOwner, Observer {
            if (!it.isNullOrEmpty()) {
                Toast.makeText(context, it, Toast.LENGTH_LONG).show()
            }
        })
        viewModel.isLoading.observe(viewLifecycleOwner, Observer<Boolean> {
            progressBar.visibility = if (it) View.VISIBLE else View.GONE
        })
        viewModel.post.observe(viewLifecycleOwner, Observer {
            Picasso.get().load(it.image).placeholder(androidx.fragment.R.drawable.notification_bg)
                .into(imageView)
            tvName.text = it.name
            tvDescription.text = it.description
        })
        viewModel.comments.observe(viewLifecycleOwner, Observer{
            commentsAdapter.refreshData(it)
        })
    }
    private fun showAddCommentDialog(){

    }
}