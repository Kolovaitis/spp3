package by.kolovaitis.spp.presentation.post

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import by.kolovaitis.spp.domain.model.Comment
import by.kolovaitis.spp.domain.model.Post
import by.kolovaitis.spp.domain.usecase.AddCommentUsecase
import by.kolovaitis.spp.domain.usecase.GetCommentsUsecase
import by.kolovaitis.spp.domain.usecase.GetPostUsecase
import by.kolovaitis.spp.presentation.base.BaseViewModel
import kotlinx.coroutines.launch

class PostViewModel(
    private val getPost: GetPostUsecase,
    private val getComments: GetCommentsUsecase,
    private val addCommentUsecase: AddCommentUsecase
) : BaseViewModel() {
    private val _comments: MutableLiveData<List<Comment>> = MutableLiveData()
    val comments: LiveData<List<Comment>> get() = _comments

    private val _post: MutableLiveData<Post> = MutableLiveData()
    val post: LiveData<Post> get() = _post

    fun load(id: Int?) {
        viewModelScope.launch {
            runSafe {
                id?.let {
                    _post.postValue(getPost(id)) }
            }
        }
    }

    fun loadComments(id: Int?) {
        viewModelScope.launch {
            runSafe {
                id?.let { _comments.postValue(getComments(id)) }
            }
        }
    }
    fun addComment(id:Int?, text:String){
        viewModelScope.launch {
            runSafe {
                id?.let {
                    addCommentUsecase(postId = id, text = text)
                    _comments.postValue(getComments(id)) }
            }
        }
    }
}