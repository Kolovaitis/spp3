package by.kolovaitis.spp.presentation.list

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import by.kolovaitis.spp.R
import by.kolovaitis.spp.domain.model.Post
import com.squareup.picasso.Picasso

class PostsRecyclerViewAdapter(private var posts: List<Post>, private val onClickAction:(postId:Int)->Unit) :
    RecyclerView.Adapter<PostsRecyclerViewAdapter.PostViewHolder>() {
    class PostViewHolder(val view: View) : RecyclerView.ViewHolder(view) {
        val tvName: TextView by lazy {
            view.findViewById<TextView>(R.id.tvName)
        }
        val imageView: ImageView by lazy {
            view.findViewById<ImageView>(R.id.imageView)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PostViewHolder {
        val itemView =
            LayoutInflater.from(parent.context).inflate(R.layout.item_post, parent, false)
        return PostViewHolder(itemView)
    }

    override fun getItemCount() = posts.size

    override fun onBindViewHolder(holder: PostViewHolder, position: Int) {
        Picasso.get().load(posts[position].image)
            .placeholder(androidx.fragment.R.drawable.notification_bg).into(holder.imageView)
        holder.tvName.text = posts[position].name
        holder.view.setOnClickListener {onClickAction(posts[position].id!!)}
    }
    fun refreshData(data:List<Post>){
        posts = data
        this.notifyDataSetChanged()
    }
}