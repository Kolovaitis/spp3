package by.kolovaitis.spp.presentation.login

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import androidx.core.os.bundleOf
import androidx.navigation.fragment.findNavController
import by.kolovaitis.spp.R
import kotlinx.android.synthetic.main.fragment_login.*
import org.koin.androidx.viewmodel.ext.android.viewModel


class LoginFragment : Fragment() {
    private val vm: LoginViewModel by viewModel()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_login, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        vm.isAuthorize.observe(viewLifecycleOwner) {
            if (it) {
                findNavController().navigate(
                    R.id.action_loginFragment_to_FirstFragment
                )
            }
        }
        vm.isLoading.observe(viewLifecycleOwner) {
            bRegister.isEnabled = !it
            bLogin.isEnabled = !it
        }
        vm.error.observe(viewLifecycleOwner) {
            Toast.makeText(context, it, Toast.LENGTH_LONG).show()
        }
        bRegister.setOnClickListener {
            vm.register(etEmail.text.toString(), etPassword.text.toString())
        }
        bLogin.setOnClickListener {
            vm.login(etEmail.text.toString(), etPassword.text.toString())
        }

    }


}