package by.kolovaitis.spp.presentation.list

import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.ViewGroup.LayoutParams.WRAP_CONTENT
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.os.bundleOf
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import by.kolovaitis.spp.R
import com.afollestad.materialdialogs.MaterialDialog
import com.afollestad.materialdialogs.bottomsheets.BasicGridItem
import com.afollestad.materialdialogs.bottomsheets.BottomSheet
import com.afollestad.materialdialogs.bottomsheets.gridItems
import com.afollestad.materialdialogs.customview.customView
import com.afollestad.materialdialogs.customview.getCustomView
import com.afollestad.materialdialogs.lifecycle.lifecycleOwner
import com.google.android.material.snackbar.Snackbar
import org.koin.androidx.viewmodel.ext.android.viewModel
import kotlinx.android.synthetic.main.fragment_first.*

/**
 * A simple [Fragment] subclass as the default destination in the navigation.
 */
class ListFragment : Fragment() {
    private val viewModel: ListViewModel by viewModel()
    private lateinit var postsAdapter: PostsRecyclerViewAdapter
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_first, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        postsAdapter = PostsRecyclerViewAdapter(listOf()) { postId ->
            findNavController().navigate(
                R.id.action_FirstFragment_to_SecondFragment,
                bundleOf("id" to postId)
            )
        }
        setupViews()
        observeViewModel()
    }

    private fun setupViews() {
        rvPosts.layoutManager = LinearLayoutManager(context)
        rvPosts.adapter = postsAdapter
        bAddPost.setOnClickListener {
            showAddPostDialog()
        }
    }

    private fun showAddPostDialog() {

        MaterialDialog(requireContext(), BottomSheet()).show {
            setTitle("Add post")

            customView(R.layout.add_post_layout, scrollable = true, horizontalPadding = true)
            positiveButton { dialog ->
                // Pull the password out of the custom view when the positive button is pressed
                val customView: View = dialog.getCustomView()
                val image = customView.findViewById<EditText>(R.id.etImage).text.toString()
                val name = customView.findViewById<EditText>(R.id.etName).text.toString()
                val description = customView.findViewById<EditText>(R.id.etDescription).text.toString()
                viewModel.addPost(image, name, description)
            }
            negativeButton(android.R.string.cancel)
            lifecycleOwner(viewLifecycleOwner)
        }
    }

    private fun observeViewModel() {
        viewModel.posts.observe(viewLifecycleOwner, Observer {
            postsAdapter.refreshData(it)
        })
        viewModel.error.observe(viewLifecycleOwner, Observer {
            if (!it.isNullOrEmpty()) {
                Toast.makeText(context, it, Toast.LENGTH_LONG).show()
            }
        })
        viewModel.isLoading.observe(viewLifecycleOwner, Observer {
            progressBar.visibility = if (it) View.VISIBLE else View.GONE
        }
        )
    }
}