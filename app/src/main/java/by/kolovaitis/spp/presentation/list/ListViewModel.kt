package by.kolovaitis.spp.presentation.list

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import by.kolovaitis.spp.domain.model.Post
import by.kolovaitis.spp.domain.usecase.AddPostUsecase
import by.kolovaitis.spp.domain.usecase.GetPostsUsecase
import by.kolovaitis.spp.presentation.base.BaseViewModel
import kotlinx.coroutines.launch

class ListViewModel(private val getPosts: GetPostsUsecase, private val addPostUsecase: AddPostUsecase) :
    BaseViewModel() {
    private val _posts: MutableLiveData<List<Post>> = MutableLiveData()
    val posts: LiveData<List<Post>> get() = _posts

    init {
        refreshPosts()
    }

    fun refreshPosts() {
        viewModelScope.launch {
            _posts.postValue(emptyList())
            runSafe {
                _posts.postValue(getPosts())
            }
        }
    }

    fun addPost(image: String, name: String, description: String) {
        viewModelScope.launch {
            runSafe {
                addPostUsecase(image, name, description)
            }
            _posts.postValue(emptyList())
            runSafe {
                _posts.postValue(getPosts())
            }
        }
    }
}