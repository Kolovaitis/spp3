package by.kolovaitis.spp

import android.app.Application
import by.kolovaitis.spp.modules.componentsModule
import by.kolovaitis.spp.modules.networkModule
import org.koin.core.context.startKoin

class SppApplication: Application() {
    override fun onCreate() {
        super.onCreate()
        startKoin {
            modules(listOf(networkModule, componentsModule))
        }
    }
}